/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
//#include <device.h>
//#include <LED_RGB.h>
#include <math.h>
#include <stdio.h>
extern volatile uint8 record_flag;
extern volatile uint8 playback_flag;
extern volatile uint16 idx;
extern uint16 CapSense_CSD_sensorSignal[CapSense_CSD_TOTAL_SENSOR_COUNT];

#define INDEX_MAX 1000
uint16 Tone_Comp[INDEX_MAX],Mix_Comp[INDEX_MAX],Tone_Period[INDEX_MAX],Mix_Period[INDEX_MAX];
uint32 Duration[INDEX_MAX];
uint16 tone_period_string[INDEX_MAX];
/* Capsense constants */
#define PROX_RANGE_INIT     100
#define UINT16_MAX          65535
#define NOISE_THRESH		5

/* Music constants */
#define PITCH_CUTOFF_THRESHOLD		0x000F
#define VOL_CUTOFF_THRESHOLD		0x0002
#define FREQ_A4						440 /* hz */
#define	NOTES_TO_OCTAVE				12
#define NOTES_TO_JARRING			2
#define	NOTES_TO_THIRD				4
#define ADJ_NOTES_FREQ_MULTIPLIER	1.0595	/* 1.0595^12 = 2 */
#define OCTAVE_FREQ_MULTIPLIER		2
#define DISCRETE_LOWEST_NOTE		50
#define	DISCRETE_NOTE_RANGE			45
#define DISCRETE_HIGHEST_NOTE		(DISCRETE_LOWEST_NOTE + DISCRETE_NOTE_RANGE)
#define PITCH_CLOCK_FREQ			200000	/* khz */ /* NOTE!! This value needs to be changed if the freq of Pitch_Clock is changed on the schematic */

/* globals */
CYBIT discrete = 1;

/* ISR changes play mode from continuous to discrete (toggles discrete) */
//CY_ISR(SWITCH_PRESS)
//{
//	discrete = !discrete; 
//}
int main(void)
{
    CyGlobalIntEnable; /* Enable global interrupts. */
    /* Place your initialization/startup code here (e.g. MyInst_Start()) */
    LCD_Start();
    Timer_Start();
    UART_Start();
    RECORD_SW_ISR_Start();
    PLAYBACK_SW_ISR_Start();
    record_flag = 0;
    playback_flag = 0;
    idx = 0;
    
    for(;;)
    {
        /* Place your application code here. */
        /* Variable declaration */
    uint8 x;
	int16 proximityCounts, proximityMax=1, volumeCounts, volumeMax=1, prox_prev, prox_pprev;
//	uint16 hue, sat, val=0;
	uint32 tone;
	float midi[127];	 /* holds midi note array */
	float a = FREQ_A4; /* A4 (midi[69]) is 440 hz */
	
    /* Initial delay to allow user to press reset and get away from the sensor */
//    CyDelay(2000);

    CyGlobalIntEnable;
	
	/* This for loop sets up all the midi notes, i.e. midi[MIDI_NOTE_NUMBER] = frequency of that note 
	 * Source: http://subsynth.sourceforge.net/midinote2freq.html
	 * A better implementation may be to save these as a macro LUT
	 * Idea: MIDI note numbers sent to the computer could be used to generate music/effects in the computer!
	 */
	for (x=0; x<127; x++)
	{
	   midi[x] = ((float)a/(float)32)*(pow(2,(((float)x-9)/(float)12)));
	}

    /* Start components */
    /* Start LED_RGB module */
//    LED_RGB_Start();
//	PlayMode_ISR_StartEx(SWITCH_PRESS);
	PWM_Tone_Start();
	PWM_Tone_Mix_Start();
    PWM_Tone_WriteCompare(0);		/* silence */
	PWM_Tone_Mix_WriteCompare(0);	/* silence */

    /* Start CSD proximity sensor */
    CapSense_CSD_Start();
    CapSense_CSD_EnableWidget(CapSense_CSD_SENSOR_PROXIMITYSENSOR0_0__PROX);	/* pitch proximity sensor */
    CapSense_CSD_EnableWidget(CapSense_CSD_SENSOR_PROXIMITYSENSOR1_0__PROX);	/* volume proximity sensor */
	CapSense_CSD_InitializeAllBaselines();
    
    /* Start UART */
//    UART_Start();
    /* Send "initial" character over UART */
//    UART_PutCRLF('I');

    /* Perform initial proximity read to set max and min */
    CapSense_CSD_UpdateEnabledBaselines();    
	/* Start scanning all enabled sensors */
	CapSense_CSD_ScanEnabledWidgets();
    /* Wait for scanning to complete */
	while(CapSense_CSD_IsBusy() != 0);
    
    /* Set initial limits */
    proximityMax = PROX_RANGE_INIT;

    /* Endless loop of proximity updates */
    for(;;)
    {  
        /* Read capsense proximity sensor */
        /* Update all baselines */
        CapSense_CSD_UpdateEnabledBaselines();
	    /* Start scanning all enabled sensors */
	    CapSense_CSD_ScanEnabledWidgets();
        /* Wait for scanning to complete */
	    while(CapSense_CSD_IsBusy() != 0);
		proximityCounts = CapSense_CSD_sensorSignal[0]- NOISE_THRESH;
        volumeCounts = CapSense_CSD_sensorSignal[1]- NOISE_THRESH ;
        /* Floor the counts so no negative values are displayed */
        if(proximityCounts<0)
        {
            proximityCounts=0;
        }
		if(volumeCounts<0)
        {
            volumeCounts=0;
        }
        /* Set new limits if appropriate */
        if(proximityCounts>proximityMax)
        {
            proximityMax = proximityCounts;
        }
		/* Set new limits if appropriate */
        if(volumeCounts > volumeMax)
        {
            volumeMax = volumeCounts;
        }
        /* Calculate scaled color values and write it */
//        hue = ((uint32) proximityCounts)*UINT16_MAX/proximityMax;	// hue indicates pitch
//        sat = 65535;												// always want full color saturation
//		val = ((uint32) volumeCounts)*UINT16_MAX/volumeMax;			// value (brightness) indicates volume
        
//        LED_RGB_SetColorCircleHSV(hue, sat, val);
        
        /* Print the capsense proximity to the UART for reading in Bridge Control Panel */
        /* Read with BCP command "RX8 [h=43] @1prox @0prox" */
//        UART_PutChar('C');	/* 0x43 is ascii C */
//        UART_PutChar(proximityCounts>>8);
//        UART_PutChar(proximityCounts&0xff); 
        /* Delay to keep UART traffic down */
//        CyDelay(5);
		/* some FIR (does it really matter?) */		
		tone = (0.2*(float)prox_pprev + 0.6*(float)prox_prev + 0.2*(float)proximityCounts);
		/* Only if the volume proximity sensor is above some threshold, then turn on the music! */
		if(volumeCounts < VOL_CUTOFF_THRESHOLD)
		{
			    PWM_Tone_WriteCompare(0);
			    PWM_Tone_Mix_WriteCompare(0);
            if ((record_flag == 1 ) && (idx < INDEX_MAX)) //if idx > array size we need to stop recording to not overflow buffer  
            {
                Tone_Comp[idx] = 0;
                Mix_Comp[idx] = 0;
                Tone_Period[idx] = PWM_Tone_ReadPeriod();
                Mix_Period[idx] = PWM_Tone_Mix_ReadPeriod();
                Duration[idx] = (Timer_ReadPeriod() - Timer_ReadCounter());
                idx++;
            }            
		}
		else
		{
            int16 volume_count = volumeCounts>>4;
			PWM_Tone_WriteCompare(volume_count);
			PWM_Tone_Mix_WriteCompare(volume_count);
            if ((record_flag == 1 ) && (idx < INDEX_MAX))
            {
                Tone_Comp[idx] = volume_count;
                Mix_Comp[idx] = volume_count;
                Tone_Period[idx] = PWM_Tone_ReadPeriod();
                Mix_Period[idx] = PWM_Tone_Mix_ReadPeriod();
                Duration[idx] = (Timer_ReadPeriod() - Timer_ReadCounter());
                idx++;
            }
		}
		if(discrete)
		{
			tone = 4*log(1+tone);	/* Multiplier 4 is empirically determined - feel free to play around with it */
			tone = DISCRETE_LOWEST_NOTE + tone%(DISCRETE_NOTE_RANGE); /* don't really want more than DISCRETE_NOTE_RANGE distinguishable notes, starting from midi[DISCRETE_LOWEST_NOTE] */
            
            int16 tone_period =(float)PITCH_CLOCK_FREQ/midi[tone];
            int16 tone_mix_period = (float)PITCH_CLOCK_FREQ/midi[tone + NOTES_TO_OCTAVE];
			PWM_Tone_WritePeriod(tone_period);
			PWM_Tone_Mix_WritePeriod(tone_mix_period); /* +12 is next octave (an octave contains 12 MIDI notes) */
																				  			/* also try NOTES_TO_JARRING and NOTES_TO_THIRD */
            if((record_flag == 1 ) && (idx < INDEX_MAX))
            {
                Tone_Comp[idx] = PWM_Tone_ReadCompare();
                Mix_Comp[idx] = PWM_Tone_Mix_ReadCompare();
                Tone_Period[idx] = tone_period;
                Mix_Period[idx] = tone_mix_period;
                Duration[idx] = (Timer_ReadPeriod() - Timer_ReadCounter());
                idx++;
            }    
		}
		/* silence if pitch proximity sensor is not activated above threshold */
		if(proximityCounts < PITCH_CUTOFF_THRESHOLD)						   
		{
			PWM_Tone_WriteCompare(0);
			PWM_Tone_Mix_WriteCompare(0);
            if ((record_flag == 1 ) && (idx < INDEX_MAX))
            {
                Tone_Comp[idx] = 0;
                Mix_Comp[idx] = 0;
                Tone_Period[idx] = PWM_Tone_ReadPeriod();
                Mix_Period[idx] = PWM_Tone_Mix_ReadPeriod();
                Duration[idx] = (Timer_ReadPeriod() - Timer_ReadCounter());
                idx++;
            }
		}
		if (playback_flag == 1)
        {
            LCD_Position(0,0);
            LCD_ClearDisplay();
            LCD_PrintString("Playback");
            for (int i = 0; i < idx; i++)
            {
                PWM_Tone_WriteCompare(Tone_Comp[i]);            
                PWM_Tone_Mix_WriteCompare(Mix_Comp[i]);
                PWM_Tone_WritePeriod(Tone_Period[i]);
                PWM_Tone_Mix_WritePeriod(Mix_Period[i]);
                CyDelayUs(Duration[i]);  
                //block pwm to keep the correct note for the right duration
            }
            playback_flag = 0;                          //reset playback button flag
            LCD_Position(0,0);
            LCD_ClearDisplay();
            LCD_PrintString("Playback Done");
        }
		prox_prev = proximityCounts;
		prox_pprev = prox_prev;
	}
    }
}

/* [] END OF FILE */
